# MyJS




# 帮助文档

- https://nodejs.org/dist/latest-v8.x/docs/api/



# 模块加载规则

- 先从缓存中加载

- ./
- ../
- /xxxx
- d:/xxxx
- require('foo.js')

核心模块

- require('fs')

- require('http')

第三方模块

- 不可能有第三方包名和核心模块名一样
- 先找到当前文件所处目录的node_modules目录

node_modules/art-template

node_modules/art-template/package.json文件

node_modules/art-template/package.json文件中的main属性

main属性中就记录了art-template的入口模块

然后加载使用这个第三方包

var template = require('art-template');

# 初始化项目

```javascript
npm init
```

# npm

npm网站

- npm命令
  - npm init -y 可以跳过向导，快速生成
- npm install
  - 一次性把dependences选项中的已奶香全部安装
  - npm i
- npm install 包名
  - 只下载
  - npm i 包名
- npm install --save 包名
  - 下载并且保存依赖（package.json文件中的dependencies选项）
  - npm i -S 包名
- npm uninstall 包名
  - 只删除，如果有依赖项会依然保存
  - npm un 包名
- npm uninstall --save包名
  - 删除的同名时也会把依赖信息也去除
  - npm un -S 包名

# 解决npm被墙的问题

- npm.taobao.org

安装淘宝的cnpm

```
npm install --global cnpm
```

安装包的`npm`替换成`cnpm`

```
# 这里还是走国外的npm下载jquery
npm install jquery

# 使用cnpm就会通过淘宝的服务器下载jquery
cnpm install jquery
```

# express

官网网址

- http://expressjs.com/

安装

```
npm install express --save
```

# art-template模块










