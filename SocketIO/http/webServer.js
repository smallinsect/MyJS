/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-06-03 18:20:31
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-03 18:20:39
 */
const express = require('express');
const app = express();

//开启静态资源服务器
app.use(express.static('./public'));

app.listen('8888')