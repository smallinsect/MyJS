/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-05-26 09:33:12
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-05-26 09:33:31
 */
var app = require('express').createServer();
var io = require('socket.io')(app);

app.listen(80);

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
        socket.on('my other event', function (data) {
        console.log(data);
    });
});
