/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-06-03 21:24:25
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-04 14:54:53
 */
var net = require('net');

// 创建一个套接字
var client = new net.Socket();
// 连接ip地址为127.0.0.1端口为8888的服务器
client.connect(8888, '127.0.0.1', function(){
	console.log('连接服务器成功...');

	// 每2秒向服务器发送消息。
	client.setTimeout(2*1000);
	client.on('timeout', function(){
		client.write('我是小昆虫客户端...');
	});
	// 接受服务端发来的消息
	client.on('data', function(data){
		console.log('[server]', data.toString());
	});
	// 关闭连接
	client.on('close', function(err){
		console.log('[server] close ...');
		console.log('[server]', err);
	});
	// 断开连接
	// client.destroy();
});

