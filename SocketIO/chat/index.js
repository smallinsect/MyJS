/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-05-26 20:43:15
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-05-26 21:07:29
 */
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    // 接受客户端发来的chat message事件
    socket.on('chat message', function(msg){
        console.log('message: ' + msg);
        // 广播除socket外的其他用户
        // socket.broadcast.emit('hi');
        // 广播消息
        io.emit('chat message', msg);
    });
    // 接受客户端断开事件
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

http.listen(3333, function(){
    console.log('listening on *:3333');
});
