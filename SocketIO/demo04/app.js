/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-05-26 09:38:09
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-05-26 09:41:10
 */
// 提示： io(<port>) 会自动创建 http 服务器
var io = require('socket.io')(80);
var chat = io
    .of('/chat')
    .on('connection', function (socket) {
        socket.emit('a message', {
            that: 'only'
        , '/chat': 'will get'
        });
        chat.emit('a message', {
            everyone: 'in'
        , '/chat': 'will get'
        });
    });

var news = io
    .of('/news')
    .on('connection', function (socket) {
        socket.emit('item', { news: 'item' });
    });