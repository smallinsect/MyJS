/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-07-03 11:01:52
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-07-03 11:27:19
 */
// 读写文件模块
var fs = require('fs')

var map = {
    '[1]': 5, '[2]': 5, '[3]': 10, '[4]': 5, '[5]': 5, '[6]': 5, '[7]': 5, '[8]': 5, '[8]': 5,'[9]': 5,'[10]': 5,
    '[10]': 5, '[11]': 5, '[12]': 5, '[13]': 5, '[14]': 5, '[15]': 5, '[16]': 5, '[17]': 5, '[18]': 5,'[19]': 5,'[20]': 5,
    '[21]': 5,
    '[101]': 50, '[102]': 50, '[103]': 50, '[104]': 50,
    '[201]': 150, '[202]': 150, '[203]': 150, '[204]': 150, '[205]': 150, '[206]': 150,
};

// 读取文件
fs.readFile('./player-5.log', function(error, data) {
    if (error) {
        console.log(error)
        return
    }
    var str = data.toString()
    // console.log(str)
    var strArr = str.split('\n')
    // console.log(strArr)
    // var result = {};
    // for (var i = 0; i < strArr.length; ++i) {
    //     var one = strArr[i];
    //     if (result[one] == undefined) {
    //         result[one] = 1;
    //     } else {
    //         result[one]++;
    //     }
    // }
    // console.log(result)
    var result = 0;
    for (var i = 0; i < strArr.length; ++i) {
        var one = strArr[i];
        if (map[one]) {
            result += map[one];
        }
    }
    console.log(result);
})
