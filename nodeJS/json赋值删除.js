/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-07-08 15:52:19
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-07-08 15:53:54
 */
let a1 = {'x1':'x1', 'x2':'x2'};
let a2 = {};
a2['y1'] = a1;
a2['y2'] = a1;

console.log(a1);
console.log(a2);

delete a2['y1']['x1']

console.log('===========================');
console.log(a1);
console.log(a2);