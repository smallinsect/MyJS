/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-08-13 15:24:44
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-08-13 15:29:36
 */

var arr1 = [1, 2, 3, 4];

console.log(!arr1.indexOf(1));
console.log(!arr1.indexOf(-1));
console.log(!arr1.indexOf(5));
