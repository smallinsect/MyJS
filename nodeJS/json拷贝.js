/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-08-13 14:32:51
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-08-13 14:34:19
 */

var obj1 = {'name1': 11, 'name2': 22};
var obj2 = {'val1': 33, 'val2': 44};

var obj = Object.assign(obj1, obj2);

console.log(obj);
console.log(obj1);
console.log(obj2);