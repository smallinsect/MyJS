/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-06-21 18:15:14
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-21 18:24:08
 */

var assetState = { 1:'正常', 2:'故障', 3:'报废', 4:'维修', 5:'包养' };
function returnAssetState(val){
    return assetState[val];
}

var assetType = { '1':'电子设备', '2':'生产设备', '3':'办公设备' };
function returnAssetType(val){
    return assetType[val];
}

var useState = { 1:'使用中', 2:'闲置', 3:'借用中', 4:'借出中', 5:'超期未还', 6:'归还代收' };
function returnUseState(val){
    return returnUseState[val];
}

console.log(returnAssetState(1));
console.log(returnAssetState(2));
console.log(returnAssetState(3));
console.log(returnAssetState(6));
