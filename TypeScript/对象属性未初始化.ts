/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-04-21 20:19:10
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-21 20:22:21
 */

interface Animal {
    name: string;
    age: number;
    egg?: number;
}

let animal: Animal = <Animal>{};
console.log('------------------------');
console.log(animal.name);
console.log('------------------------');
console.log(animal.age);
console.log('------------------------');
console.log(animal.egg);
console.log('------------------------');
