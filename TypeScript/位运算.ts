/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-05-12 11:58:11
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-05-13 09:59:02
 */

let num: number = 1;

console.log((num << 30).toString(2));
console.log((num << 31).toString(2));
console.log((num << 32).toString(2));
console.log((num << 33).toString(2));
