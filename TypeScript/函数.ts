/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-04-14 08:58:40
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-14 09:14:45
 */

function func1(): {data: number, err: string, msg: string} {
    return { data: 1, err: "小昆虫", msg: "爱白菜" };
}

console.log(func1());
{
    let obj = func1();
    console.log('用一个变量接受函数返回的值 ', obj, obj.data, obj.err, obj.msg);
}
{
    let { data: dataT, err: errT, msg: msgT } = func1();
    console.log('用json的方式接受值 ', dataT, errT, msgT);
}

{
    let { data, err, msg } = func1();
    console.log('用json默认字段方式接受值 ', data, err, msg);
}
