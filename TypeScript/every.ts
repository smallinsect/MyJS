/*
 * @description: 
 * @version: 1.0.1
 * @Author: xuqiulin
 * @Date: 2021-04-16 15:00:13
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-16 15:04:03
 */

let arr6: number[] = [1, 2, 3, 4, 5, 6];

// 一个返回false 终止遍历
arr6.every((item, index, array) => {
    console.log(item);
    if (item > 3) {
        return true;
    }
    return false;
});

